module ApplicationHelper
	def template_fields(f,association)
		new_object  = f.object.class.reflect_on_association(association).klass.new
		template = f.fields_for(association, new_object, child_index: "replaceable_new_#{association}") do |builder|
					render(association.to_s.singularize+"_fields",f: builder)
		end
		template.to_s
	end
end
