$(function(){
	(function($){
		var _inputFieldTemplate = $("#ingredients_field").data("template"); //raw template for new ingredient
		var _addedFieldTemplate = "<input type='hidden' name='recipe[added_ingredients][]'/>"; // raw template for added ingredient field
		var _filterQueryTemplate ="<input type='hidden' name='ingredients[]'/>"; // raw template for filter query params
		
		function addEventListeners(){
			// For New and Edit Recipe form , before request is sent to the server
			$("#new_recipe, form.edit_recipe").on("submit",function(evt){
			//evt.preventDefault();
				$("#ingredients_field input").each(function(index,item){
					var ingredientId = $(this).data('id');
					var ingredientName = $(this).data("name");
					if($(item).is(":checked")){
						$(_addedFieldTemplate)
							.prependTo("#added_ingredients")
							.val(ingredientId);
					}
				});
			});

			// For addding more ingredient which are not in the list
			$("#add_new_ingredient").on("click",function(evt){
				evt.preventDefault();
				evt.stopPropagation();
				$(renderAddTemplate())
					.insertAfter(this);
			})

			// for removing fields for adding additional ingredient
			$("#ingredients_field").on("click",".icon.remove_ingredient",function(){
				$(this).closest("div.form-group").remove();
			});

			// for filter items
			$("#filter_form .filter.item").on("click",function(){
				$(this).toggleClass("selected");
			});

			//for filter form, before request is made to the server
			$("#filter_form").on("submit",function(evt){
				//evt.preventDefault();
				var form = $(this);
				$(this).find(".filter.selected").each(function(index,item){
					var search_key = $(item).data("key");
					$(_filterQueryTemplate).appendTo(form).val(search_key);
				});
			});
		}

		// template for a new ingredient
		function renderAddTemplate(){
			var new_id = new Date().getTime();
			var regexp = new RegExp("replaceable_new_ingredients", "g")
			return _inputFieldTemplate.replace(regexp, new_id);
		}

		addEventListeners();

	})($);
});
	