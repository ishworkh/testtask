class Recipe < ActiveRecord::Base
	include IngredientsHandler
	
	has_and_belongs_to_many :ingredients

	accepts_nested_attributes_for :ingredients, reject_if: lambda{|a| a[:name].blank?}

	validates :title, uniqueness: true

	
end
