module IngredientsHandler
	extend ActiveSupport::Concern

	included do 
		attr_accessor :added_ingredients

		before_save  do 
			@availableIngredients = Ingredient.all.to_a
		end

		after_save :checkIngredientsAssociation,on:[:create,:update]
	end

protected
	def checkIngredientsAssociation
		currentIngredients = ingredients.to_a
		currentIngredientsIds  = currentIngredients.map{|item| item.id.to_s}
		addedIngredients = added_ingredients
		# Delete remove ingredients
		if addedIngredients
			(currentIngredients & @availableIngredients).each do |item|
				unless(addedIngredients.include? item.id.to_s)
					ingredients.delete(item)
				end
			end

			# Insert added ingredients in the association
			(addedIngredients - currentIngredientsIds).each do |item|
				ingredient = Ingredient.find(item.to_i)
				ingredients << ingredient
			end
		end	
	end



end




	
	
