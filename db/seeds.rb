# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
	recipes = ["burger","sandwich","pizza","chicken soup"]
	ingredients = [["bread","onion"],["bread","cheese"],["flour","cheese","sauce"],["chicken","ginger","garlic","chilly"]]
	recipes.each_with_index do |item,index|
		recipe = Recipe.new(title: item,description:"#{item} Recipe Good One!!!")
		if (recipe.save)
			ingredients[index].each do |ingredient|
				recipe.ingredients << (Ingredient.find_or_create_by(name: ingredient))
			end
		end
	end

	