Here I have created this small app as explained in the spec, with Ruby on Rails i.e rails version 4.0.8, so might not be compatible to lower version 3.x.x.

Installation steps:

1. bundle install
2. rake db:migrate
3. rake db:seed (I have seeded some data already)
4. rails server